var menu = $('.wrapper div');

$(".menu").on('click', function(mapSwap) {
	var menuNum = $(this).data('menu');
	if (menuNum == 1) {
		dew.command('Game.PlaySound 0x0B00');
		dew.show("multiplayermenu");
		$( ".blackscreen" ).fadeIn( 300, function() {
			dew.hide("forgemenu");    
		});
	}
	else if (menuNum == 2) {
		dew.command('Game.PlaySound 0x0B00');
		dew.show("server_settings");
	}
	else if (menuNum == 3) {
		dew.command('game.map s3d_turf');
		dew.notify("mapChange");
	}
	else if (menuNum == 5) {
		dew.command('Game.PlaySound 0x0B00');
		dew.command("game.hideh3ui 0")
		dew.command("Game.Start");
		$( ".blackscreen" ).fadeIn( 500, function() {
			dew.hide("forgemenu");    
		});
	}
})

$(".menu").mouseenter(function() {
	var menuNum = $(this).data('menu');
	if (Number.isInteger(menuNum)) {
		dew.command('Game.PlaySound 0xAFE');
	}
});

$("html").on("keydown", function(e) {
	if(e.keyCode == 192 || e.keyCode == 223){
		dew.show('console');
	}
	else if(e.keycode == 122){
		dew.show("browser");
		$(".wrapper, .backButtonBackground").hide();
	}
});

dew.on("show", function() {
	$( ".blackscreen" ).hide();
	$(".wrapper, .backButtonBackground").show();
	updateCurrentMap();
	dew.notify("forgemenuShow"); // Broadcast dew.show so other screens can detect this
});

dew.on("hide", function() {
	dew.notify("forgemenuHide"); // Broadcast dew.hide so other screens can detect this
});

dew.on("server_settingsShow", function() {
	$(".wrapper, .backButtonBackground").fadeOut(300);
});

dew.on("server_settingsHide", function() {
	$(".wrapper, .backButtonBackground").fadeIn(300);
});

dew.on("mapChange", function () {
	updateCurrentMap();
});

dew.on("console", function () {
	updateCurrentMap();
});

$("body").mouseleave(function () {
	$(".wrapper, .backButtonBackground").hide();
});

$(".bottomuiText").mouseenter(function() {
	dew.command('Game.PlaySound 0xAFE');
});
$(".bottomuiText").on('click', function() {
	var menuNum = $(this).data('menu');
	if (menuNum == 1) {
		dew.command('Game.PlaySound 0x0B00');
		dew.show("server_settings");
	}
	else if (menuNum == 2) {
		
		dew.toast({body:'This button does nothing yet'});
	}
	else if (menuNum == 3) {
		dew.show("mmenu");
	$( ".blackscreen" ).fadeIn( 300, function() {
		dew.hide("forgemenu");
	});
	dew.command('Input.UIButtonPress 1');
	}
	
});


function updateCurrentMap() {
	dew.getMapVariantInfo().then(function(i){
		if (i.name.toLowerCase() == "standoff") {
			var mapListing = "bunkerworld";
		}
		else if (i.name.toLowerCase() == "narrows") {
			var mapListing = "chill";
		}
		else if (i.name.toLowerCase() == "the pit") {
			var mapListing = "cyberdyne";
		}
		else if (i.name.toLowerCase() == "high ground") {
			var mapListing = "deadlock";
		}
		else if (i.name.toLowerCase() == "valhalla") {
			var mapListing = "riverworld";
		}
		else if (i.name.toLowerCase() == "diamondback") {
			var mapListing = "s3d_avalanche";
		}
		else if (i.name.toLowerCase() == "edge") {
			var mapListing = "s3d_edge";
		}
		else if (i.name.toLowerCase() == "reactor") {
			var mapListing = "s3d_reactor";
		}
		else if (i.name.toLowerCase() == "icebox") {
			var mapListing = "s3d_turf";
		}
		else if (i.name.toLowerCase() == "sandtrap") {
			var mapListing = "shrine";
		}
		else if (i.name.toLowerCase() == "last resort") {
			var mapListing = "zanzibar";
		}
		else {
			var mapListing = i.name.toLowerCase();
		}
		$("#mapSelection").text("MAP: "+ i.name.toUpperCase());
		$("#mapView").attr("src", "dew://assets/maps/large/" + mapListing + ".jpg");
	});
}