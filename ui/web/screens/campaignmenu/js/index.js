var menu = $('.wrapper div');

menu.on('click', function() {
	var menuNum = $(this).data('menu');
	// $(this).toggleClass('menu-'+ menuNum +'-active');
	if (menuNum == 1) {
		dew.command('Input.UIButtonPress 1');
		dew.show("mmenu");
		$( "body" ).fadeOut( 2000, function() {
			dew.hide("campaignmenu");    
		});
	}
	else if (menuNum == 2) {
		dew.command('Game.PlaySound 0x0B00');
		dew.show("server_settings");
	}
	else if (menuNum == 3) {
		dew.command('Game.PlaySound 0x0B00');
		dew.command("game.hideh3ui 0")
		dew.command("Game.Start");
		$( "body" ).fadeOut( 2000, function() {
			dew.hide("campaignmenu");    
		});
	}
})

menu.mouseenter(function() {
	var menuNum = $(this).data('menu');
	if (Number.isInteger(menuNum)) {
		dew.command('Game.PlaySound 0xAFE');
	}
});

$("html").on("keydown", function(e) {
	if(e.keyCode == 192 || e.keyCode == 223){
		dew.show('console');
	}
});