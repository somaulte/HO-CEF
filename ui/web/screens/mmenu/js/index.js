var menu = $('.wrapper div');

$(".menu").on('click', function() {
	var menuNum = $(this).data('menu');
	if (menuNum == 1) {
		dew.command('Game.PlaySound 0x0B00');
		dew.show("browser");
		$(".wrapper, .exitButtonBackground").hide();
	}
	else if (menuNum == 2) {
		//dew.command('Server.LobbyType 0');
		//dew.command('Game.PlaySound 0x0B00');
		//dew.hide("mmenu");
		//$( "body" ).fadeOut( 300, function() {
		//	dew.hide("mmenu");    
		//});
		dew.toast({body:'Campaign Menu not yet accessible'});
	}
	else if (menuNum == 3) {
		dew.command('Server.LobbyType 2');
		dew.command('Game.PlaySound 0x0B00');
		dew.show("multiplayermenu");
		$("body").fadeOut( 300, function() {
			dew.hide("mmenu");    
		});
	}
	else if (menuNum == 4) {
		dew.command('Server.LobbyType 3');	
		dew.command('Game.PlaySound 0x0B00');
		dew.show("forgemenu");
		$("blackscreen").fadeIn( 300, function() {
			dew.hide("mmenu");    
		});
	}
	else if (menuNum == 5) {
		dew.command('Game.PlaySound 0x0B00');
		dew.show("profile_settings");
		$("body").fadeOut( 300, function() {
			dew.hide("mmenu");    
		});
	}
	else if (menuNum == 6) {
		dew.show("settings");
	}
});

$(".menu").mouseenter(function() {
	var menuNum = $(this).data('menu');
	if (Number.isInteger(menuNum)) {
		dew.command('Game.PlaySound 0xAFE');
	}
});

$("html").on("keydown", function(e) {
	if(e.keyCode == 192 || e.keyCode == 223){
		dew.show('console');
	}
	else if(e.keyCode == 27){
		dew.command('game.hideh3ui 1');
		dew.hide('mmenu');
	}
	else if(e.keycode == 122){
		dew.show("browser");
		$(".wrapper, .exitButtonBackground").hide();
	}
});

dew.on('serverconnect', function(e){
    hideScreen();
});

function hideScreen(){
	dew.command('Server.LobbyType', {}).then(function(result){
		if (result == 0){
			dew.show("campaignmenu");
		}
		else if (result == 2){
			dew.show("multiplayermenu");
		}
		else if (result == 3){
			dew.show("forgemenu");
		}
	});
    $( "body" ).fadeOut( 2000, function() {
        dew.hide("mmenu");    
    });
	dew.command('Game.PlaySound 0x0B00');
}

dew.on("show", function() {
	$("body").show();
	// Fetch game version
	dew.command('Game.Version', {}).then(function(result){
		$("#versionString").text(result.toUpperCase());
	});
});

$("body").mouseenter(function() {
	$(".wrapper, .exitButtonBackground").show();
});

$(".exitButton").mouseenter(function() {
	dew.command('Game.PlaySound 0xAFE');
});

$(".exitButton").on('click', function() {
	dew.command('Game.PlaySound 0x0B00');
	$( "body" ).fadeOut( 2000, function() {
        dew.hide("mmenu");    
    });
	dew.command('Game.Exit');
});