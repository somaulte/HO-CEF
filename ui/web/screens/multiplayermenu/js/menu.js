var teamArray = [
    {name: 'red', color: '#620B0B'},
    {name: 'blue', color: '#0B2362'},
    {name: 'green', color: '#1F3602'},
    {name: 'orange', color: '#BC4D00'},
    {name: 'purple', color: '#1D1052'},
    {name: 'gold', color: '#A77708'},   
    {name: 'brown', color: '#1C0D02'}, 
    {name: 'pink', color: '#FF4D8A'},  
    {name: 'white', color: '#D8D8D8'}, 
    {name: 'black', color: '#0B0B0B'}           
];
var cardOpacity = 0.9;

var index = 0;
var useKB = false;

$(window).load(function(){
    
    dew.on("show", function(){
        dew.getSessionInfo().then(function(e){
            if(e.established){
                index = 0;
                //$("#lobby").show();
                   // function listupdate(){
                        dew.command('Server.ListPlayersJSON', {}).then(function(l){
                            var playerArray = JSON.parse(l);
                            $('#playerList').empty();
                            for(var i=0; i < playerArray.length; i++){
                                var bgColor = playerArray[i].color;
                                if(e.hasTeams){
                                    bgColor = teamArray[playerArray[i].teamIndex].color;
                                }
                                $('#playerList').append(
                                    $('<li>', {
                                        text: playerArray[i].name,
                                        css: {
                                            backgroundColor: hexToRgb(bgColor,cardOpacity)
                                        },
                                        id: playerArray[i].name,
                                        'data-color': bgColor,
                                    }).mouseover(function(){
                                        col = $(this).attr('data-color'),
                                        bright = adjustColor(col, 30);
                                        $(this).css("background-color", hexToRgb(bright, cardOpacity));
                                    }).mouseout(function(){
                                        col = $(this).attr('data-color');
                                        $(this).css("background-color", hexToRgb(col, cardOpacity));
                                    })
                                )
                                $('#'+playerArray[i].name).prepend('<img class="emblem" src="dew://assets/ed/logo.png">');
                            }
                            dew.command('Server.Mode', {}).then(function(m){
                               
                                if(e.isHost){
                                    var maxPlayers = 2;
                                    if(m = 3){
                                        dew.command('Server.MaxPlayers', {}).then(function(p){
                                            maxPlayers = p;
                                        });
                                    }
                                    $("#playerCount").text(playerArray.length + " Player  (" + maxPlayers + " max)");
                                }
                                dew.command('Server.LobbyType', {}).then(function(t){
                                    console.log('Lobby Type:' + lobbyType[t]);
                                });
                            });
                        });   
                        //setTimeout(listupdate, 500);
                    //}  
                    //listupdate();
            }
        });
    });
    
      
    $(".selectable").mouseover(function(){
        $(this).addClass("selected");
        index = $.inArray($(this)[0],list);
    }).mouseout(function(e){
        $(this).removeClass("selected");
        useKB = false;
    });
    
    $("#switchTeams").click(function(){
        dew.command('Input.UIButtonPress 4', {}).then(function(){
            setTimeout(function(){
                dew.show();
            },1500);
        });
    });
    
});

function hexToRgb(hex, opacity){
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return "rgba(" + parseInt(result[1], 16) + "," + parseInt(result[2], 16) + "," + parseInt(result[3], 16) + "," + opacity + ")";
}

function adjustColor(color, amount){
    var colorhex = (color.split("#")[1]).match(/.{2}/g);
    for (var i = 0; i < 3; i++){
        var e = parseInt(colorhex[i], 16);
        e += amount;
        if(amount > 0){
            colorhex[i] = ((e > 255) ? 255 : e).toString(16);
        }else{
            colorhex[i] = ((e < 0) ? 0 : e).toString(16);
        }
    }
    return "#" + colorhex[0] + colorhex[1] + colorhex[2];
}